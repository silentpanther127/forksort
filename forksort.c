#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <sys/queue.h>
#include <string.h>
#include <errno.h>
#include <assert.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include "forksort.h"

TAILQ_HEAD(tailhead, entry);

/**
* @brief Tries to cleanup all resources in use.
* @return void
*/
static void cleanup(void);

/**
* @brief cleans up list 1 pointed to by head_r
* @details cleans up list by freeing every struct and all 'var' ellements of the structs
* @return void
*/
static void cleanup_lst_1(void);
static void cleanup_lst_2(void);
static void cleanup_lst_r(void);

/**
* @brief Exits the programm printing an error and freeing resources;
* @param char* s a string of an error to print or NULL
* @details If s != NULL s is beeing ptinted to stderr,
  the string generated by strerror(errno) is beeing printed.
  Afterwards resources are beeing cleaned up by a call to cleanup().
  And the programm exits with exit(EXIT_FAILURE).
* @return This function does not return do to the call to exit().
*/
static void error_exit(char* s);


/**
* @brief Calculate the lists length, by traversing it
* @details this operation is performed in O(n)!
* @param head the head of the list to calculate the length of
* @return an integer of the list's length
*/
static int list_len(struct tailhead head);

/**
* @brief fill a list from a stream
* @param TAILQ_HEAD(tailhead, entry) head the head of the lsit to fill
* @param stream a stram to read from
* @details This function reads from a stream and starts populating a list with lines
*          read from the stram until EOF is reached.
* @return void
*/
static void fill_list(struct tailhead head, FILE* stream);

/**
* @brief Function to compare two of our line lists by lexographical order.
* @param TAILQ_HEAD(tailhead, entry) head_1 first list to compare
* @param TAILQ_HEAD(tailhead, entry) head_2 second list to compare
* @return true if the first list comes first in lexographical order, false otherwise.
*/
static bool lst_cmp(struct tailhead head_1, struct tailhead head_2);

/**
* @brief compare two strings for lexographical order
* @details compare two strings if one of them is NULL, the other one is
*          beeing treated as first. If they are both NULL, assert fails
* @param a string to compare
* @param b other string to compare
* @return true if a comes first in lexographical order false otherwise
*/
static bool str_order(char *a, char *b);

struct tailhead head_r = TAILQ_HEAD_INITIALIZER(head_r);

struct tailhead head_1 = TAILQ_HEAD_INITIALIZER(head_1);

struct tailhead head_2 = TAILQ_HEAD_INITIALIZER(head_2);

FILE* fpchld1w = NULL;
FILE* fpchld1r = NULL;
FILE* fpchld2w = NULL;
FILE* fpchld2r = NULL;

static void fill_list(struct tailhead head, FILE* stream){
	int r;
	do {
		char* buff = NULL;
		size_t buffLen = 0;
		
		r = getline(&buff, &buffLen, stream);
		
		/* getline() might have failed */
		if (buff != NULL){
			if (strlen(buff) > 0){
				struct entry* cur = malloc(sizeof(struct entry));
				
				if (cur == NULL){
					error_exit("malloc failed");
				}
				
				cur->val = buff;
				
				TAILQ_INSERT_TAIL(&head, cur, entries);
			} else {
				free(buff);
			}
		}
	} while (r != -1);
}

int main(int argc, char* argv[]){
	
	/* Arg handling */
	if (argc != 1){
		fprintf(stderr, "Too many arguments!\n");
		return EXIT_FAILURE;
	}
	
	TAILQ_INIT(&head_r);
	
	fill_list(head_r, stdin);
	
	int len = list_len(head_r);
	if (len == 0) {
		fprintf(stderr, "nothing to sort!");
		cleanup();
		exit(EXIT_SUCCESS);
	}
	if (len == 1) {
		struct entry* n = TAILQ_FIRST(&head_r);
		fprintf(stdout, "%s", n->val);
		cleanup();
		exit(EXIT_SUCCESS);
	}
	
	int pipefdChld1a[2];
	if (pipe(pipefdChld1a)) {
		error_exit("failed to create pipe");
	}
	
	int pipefdChld1b[2];
	if (pipe(pipefdChld1b)) {
		error_exit("failed to create pipe");
	}
	
	int pipefdChld2a[2];
	if (pipe(pipefdChld2a)) {
		error_exit("failed to create pipe");
	}
	
	int pipefdChld2b[2];
	if (pipe(pipefdChld2b)) {
		error_exit("failed to create pipe");
	}
	
	struct entry* e = TAILQ_FIRST(&head_r);
	
	pid_t pid1 = fork();
	switch (pid1) {
		case -1:
			error_exit("can not fork!");
			break;
		case 0:
			//child tasks
			if (dup2(pipefdChld1a[0], STDIN_FILENO) < 0){
				error_exit("duplication of file descriptor failed");
			}
			if (close(pipefdChld1a[1])){
				error_exit("could not close pipe file descriptor");
			}
			if (dup2(pipefdChld1b[1], STDOUT_FILENO) < 0){
				error_exit("duplication of file descriptor failed");
			}
			if (close(pipefdChld1b[0])){
				error_exit("could not close pipe file descriptor");
			}
			//TODO: this only works if the binary is in the working dir
			if (execv("./forksort", (char* []){"forksort", NULL})){
				error_exit("can not exec()");
			}
			break;
		default:
			//parent tasks
			if (close(pipefdChld1a[0])){
				error_exit("could not close pipe file descriptor");
			}
			if (close(pipefdChld1b[1])){
				error_exit("could not close pipe file descriptor");
			}
			//write into child
			if ((fpchld1w = fdopen(pipefdChld1a[1], "w")) == NULL){
				error_exit("could not open file for writing into child");
			}
			for (int i = 0; i < len/2; i++){
				fprintf(fpchld1w, "%s", e->val);
				e = TAILQ_NEXT(e, entries);
			}
			if (fclose(fpchld1w)){
				error_exit("error closing file");
			} else {
				fpchld1w = NULL;
			}
			break;
	}
	
	pid_t pid2 = fork();
	switch (pid2) {
		case -1:
			error_exit("can not fork!");
			break;
		case 0:
			//child tasks
			if (dup2(pipefdChld2a[0], STDIN_FILENO) < 0){
				error_exit("duplication of file descriptor failed");
			}
			if (close(pipefdChld2a[1])){
				error_exit("could not close pipe file descriptor");
			}
			if (dup2(pipefdChld2b[1], STDOUT_FILENO) < 0){
				error_exit("duplication of file descriptor failed");
			}
			if (close(pipefdChld2b[0])){
				error_exit("could not close pipe file descriptor");
			}
			//TODO: this only works if the binary is in the working dir
			if (execv("./forksort", (char* []){"forksort", NULL})){
				error_exit("can not exec()");
			}
			break;
		default:
			//parent tasks
			if (close(pipefdChld2a[0])){
				error_exit("could not close pipe file descriptor");
			}
			if (close(pipefdChld2b[1])){
				error_exit("could not close pipe file descriptor");
			}
			//write into child
			if ((fpchld2w = fdopen(pipefdChld2a[1], "w")) == NULL){
				error_exit("could not open file for writing into child");
			}
			while (e != NULL){
				fprintf(fpchld2w, "%s", e->val);
				e = TAILQ_NEXT(e, entries);
			}
			if (fclose(fpchld2w)){
				error_exit("error closing file");
			} else {
				fpchld2w = NULL;
			}
			break;
	}
	
	//read from both children into 2 lists
	//wait for the children
	//look for the alphabetical order
	// -> this is the actual sorting process
	//write them to stdout (ordered)
	
	TAILQ_INIT(&head_1);
	if ((fpchld1r = fdopen(pipefdChld1b[0], "r")) == NULL){
		error_exit("could not open file for reading from child");
	}
	fill_list(head_1, fpchld1r);
	
	TAILQ_INIT(&head_2);
	if ((fpchld2r = fdopen(pipefdChld2b[0], "r")) == NULL){
		error_exit("could not open file for reading from child");
	}
	fill_list(head_2, fpchld2r);
	
	//TODO: check if this is proper wait() solution
	pid_t w1;
	int w1_status;
	do {
		w1 = waitpid(pid1, &w1_status, 0);
		if (w1 == -1){
			if (errno == EINTR){
				continue;
			} else {
				//TODO: check particculary this
				error_exit("could not wait for child!");
			}
		}
	} while (!WIFEXITED(w1_status));
	
	//TODO: check if this is proper wait() solution
	pid_t w2;
	int w2_status;
	do {
		w2 = waitpid(pid2, &w2_status, 0);
		if (w2 == -1){
			if (errno == EINTR){
				continue;
			} else {
				//TODO: check particculary this
				error_exit("could not wait for child!");
			}
		}
	} while (!WIFEXITED(w2_status));
	
	struct entry *n1, *n2;
	n1 = TAILQ_FIRST(&head_1);
	n2 = TAILQ_FIRST(&head_2);
	while (n1 != NULL || n2 != NULL){
		if(n1 != NULL && n2 != NULL){
			bool b = str_order(n1->val, n2->val);
			if(b){
				fprintf(stdout, "%s", n1->val);
				n1 = TAILQ_NEXT(n1, entries);
			} else {
				fprintf(stdout, "%s", n2->val);
				n2 = TAILQ_NEXT(n2, entries);
			}
		} else {
			if(n1 == NULL){
				fprintf(stdout, "%s", n2->val);
				n2 = TAILQ_NEXT(n2, entries);
			} else {
				fprintf(stdout, "%s", n1->val);
				n1 = TAILQ_NEXT(n1, entries);
			}
		}
	}
	
	cleanup();
	return EXIT_SUCCESS;
}

static bool str_order(char *a, char *b){
	if (a == NULL && b == NULL){
		assert(0);
	} else if (a == NULL && b != NULL){
		return false;
	} else if (a != NULL && b == NULL){
		return true;
	} else {
		int i = 0;
		while (true){
			if(a[i] == '\0' && b[i] == '\0'){
				return true; //they were identical, so who cares what we return
			}
			if(a[i] == '\0'){
				return true;
			}
			if(b[i] == '\0'){
				return false;
			}
			if(a[i] != b[i]){
				return a[i] < b[i];
			}
		}
	}
}

static bool lst_cmp(struct tailhead head_1, struct tailhead head_2){
	struct entry *n1 = TAILQ_FIRST(&head_1);
	struct entry *n2 = TAILQ_FIRST(&head_2);
	
	while (n1 != NULL && n2 != NULL) {
		for (int i = 0; true ;i++){
			if (n1->val[i] != n2->val[i]){
				return n1->val[i] < n2->val[i];
			}
			if (n1->val[i] == '\0' && n1->val[i] == '\0'){
				break;
			}
			//if we get stuck on length difference, the shorter one is first in lexographical order
			return n1->val[i] == '\0';
		}
		n1 = TAILQ_NEXT(n1, entries);
		n2 = TAILQ_NEXT(n2, entries);
	}
	
	//if we get stuck on line count difference, the shorter one is first in lexographical order
	return n1 == NULL;
}

static void error_exit(char* s){
	fprintf(stderr, "ERROR: %s: %s\n", s?s:"", strerror(errno));
	cleanup();
	exit(EXIT_FAILURE);
}

static void cleanup(void){
	cleanup_lst_r();
	cleanup_lst_1();
	cleanup_lst_2();
	if (fpchld1r != NULL){
		if(fclose(fpchld1r)){
			fprintf(stderr, "trouble closing file during cleanup: %s (continuging)", strerror(errno));
		}else{
			fpchld1r = NULL;
		}
	}
	if (fpchld1w != NULL){
		if(fclose(fpchld1w)){
			fprintf(stderr, "trouble closing file during cleanup: %s (continuging)", strerror(errno));
		}else{
			fpchld1w = NULL;
		}
	}
	if (fpchld2r != NULL){
		if(fclose(fpchld2r)){
			fprintf(stderr, "trouble closing file during cleanup: %s (continuging)", strerror(errno));
		}else{
			fpchld2r = NULL;
		}
	}
	if (fpchld2w != NULL){
		if(fclose(fpchld2w)){
			fprintf(stderr, "trouble closing file during cleanup: %s (continuging)", strerror(errno));
		}else{
			fpchld2w = NULL;
		}
	}
	return;
}

static void cleanup_lst_r(void){
	struct entry *n1, *n2;
	n1 = TAILQ_FIRST(&head_r);
	while (n1 != NULL) {
		n2 = TAILQ_NEXT(n1, entries);
		free(n1->val);
		free(n1);
		n1 = n2;
	}
	return;
}

static void cleanup_lst_1(void){
	struct entry *n1, *n2;
	n1 = TAILQ_FIRST(&head_1);
	while (n1 != NULL) {
		n2 = TAILQ_NEXT(n1, entries);
		free(n1->val);
		free(n1);
		n1 = n2;
	}
	return;
}

static void cleanup_lst_2(void){
	struct entry *n1, *n2;
	n1 = TAILQ_FIRST(&head_2);
	while (n1 != NULL) {
		n2 = TAILQ_NEXT(n1, entries);
		free(n1->val);
		free(n1);
		n1 = n2;
	}
	return;
}

static int list_len(struct tailhead head){
	int i = 0;
	struct entry* np;
	TAILQ_FOREACH(np, &head, entries)
		i++;
	return i;
}
