#/*!
#* @programm auth-td
#* @author Leonhard Alton 1624280
#*/

CC = gcc -lrt -pthread
DEFS = -D_DEFAULT_SOURCE -D_POSIX_C_SOURCE=200809L
CFLAGS = -Wall -g -std=c99 -pedantic $(DEFS)

.PHONY: all clean
all: forksort

forksort: forksort.o forksort.h
	$(CC) -o $@ $^

forksort.o: forksort.c forksort.h
	$(CC) $(CFLAGS) -c -o $@ $<

clean:
	rm -rf *.o forksort
